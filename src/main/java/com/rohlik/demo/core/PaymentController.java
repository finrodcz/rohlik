package com.rohlik.demo.core;

import com.rohlik.demo.core.payment.PaymentGateway;
import com.rohlik.demo.db.entity.OrderEntity;
import com.rohlik.demo.db.entity.OrderItemEntity;
import com.rohlik.demo.db.entity.ProductEntity;
import com.rohlik.demo.db.model.Payment;
import com.rohlik.demo.db.repo.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.time.Instant;

@Component
public class PaymentController {

    private final OrderRepository orderRepository;

    private final PaymentGateway paymentGateway;

    @Autowired
    public PaymentController(OrderRepository orderRepository,
                             PaymentGateway paymentGateway) {
        this.orderRepository = orderRepository;
        this.paymentGateway = paymentGateway;
    }

    @Transactional
    public Payment createPayment(Payment payment) {
        OrderEntity orderEntity =
                orderRepository.findById(payment.getOrderId()).orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Order with id '%s' not found", payment.getOrderId())
                ));

        checkOrder(orderEntity);

        // this is dumb
        boolean paymentSuccessful = paymentGateway.payStuff(orderEntity.getPrice());

        if (paymentSuccessful) {
            orderEntity.getOrderItems().forEach(this::adjustAvailableProduct);
            orderEntity.setOrderComplete(true);

            return new Payment(
                    orderEntity.getId(),
                    true
            );
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Payment not accepted.");
        }
    }

    private void checkOrder(OrderEntity orderEntity) {
        if (orderEntity.isOrderComplete()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Order already completed.");
        }

        Instant now = Instant.now();
        if (orderEntity.getOrderTime().isAfter(now)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Order is expired.");
        }
    }

    private void adjustAvailableProduct(OrderItemEntity orderItemEntity) {
        ProductEntity productEntity = orderItemEntity.getProduct();
        int orderCount = orderItemEntity.getCount();
        int productCount = productEntity.getCount();
        int productOrderedCount = productEntity.getOrdered();

        productEntity.setCount(productCount - orderCount);
        productEntity.setOrdered(productOrderedCount - orderCount);
    }

}
