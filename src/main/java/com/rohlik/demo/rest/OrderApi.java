package com.rohlik.demo.rest;

import com.rohlik.demo.core.OrderController;
import com.rohlik.demo.db.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderApi {

    private final OrderController orderController;

    @Autowired
    public OrderApi(OrderController orderController) {
        this.orderController = orderController;
    }

    @GetMapping
    public List<Order> getAll() {
        return orderController.getAll();
    }

    @GetMapping("/{id}")
    public Order getOne(@PathVariable("id") Long id) {
        return orderController.getOne(id);
    }

    @PostMapping
    public ResponseEntity<Object> createOrder(@Valid @RequestBody Order order) {
        return orderController.createOrder(order);
    }

    @DeleteMapping("/{id}")
    public Order cancelOrder(@PathVariable("id") Long id) {
        return orderController.cancelOrder(id);
    }

}
