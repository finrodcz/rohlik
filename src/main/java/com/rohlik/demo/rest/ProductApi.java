package com.rohlik.demo.rest;

import com.rohlik.demo.core.ProductController;
import com.rohlik.demo.db.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductApi {

    private final ProductController productController;

    @Autowired
    public ProductApi(ProductController productController) {
        this.productController = productController;
    }

    @GetMapping
    public List<Product> getAll() {
        return productController.getAll();
    }

    @GetMapping("/{id}")
    public Product getOne(@PathVariable("id") Long id) {
        return productController.getOne(id);
    }

    @PostMapping
    public Product create(@Valid @RequestBody Product product) {
        return productController.createProduct(product);
    }

    @PutMapping("/{id}")
    public Product update(@PathVariable("id") Long id,
                          @Valid @RequestBody Product product) {
        return productController.updateProduct(id, product);
    }
}
