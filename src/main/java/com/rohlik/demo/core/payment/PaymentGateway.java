package com.rohlik.demo.core.payment;

import com.rohlik.demo.db.model.Payment;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

public interface PaymentGateway {
    boolean payStuff(BigDecimal cost);
}
