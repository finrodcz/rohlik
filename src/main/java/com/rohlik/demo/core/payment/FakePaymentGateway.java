package com.rohlik.demo.core.payment;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class FakePaymentGateway implements PaymentGateway{

    private static final BigDecimal MONEY_ON_ACCOUNT = BigDecimal.valueOf(Long.MAX_VALUE);

    @Override
    public boolean payStuff(BigDecimal cost) {
        // very fake payment success
        if (cost.compareTo(MONEY_ON_ACCOUNT) < 0) {
            return true;
        } else {
            return false;
        }
    }

}
