package com.rohlik.demo.core.scheduled;

import com.rohlik.demo.core.OrderController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class OrderExpirationCheck {

    private final Logger logger = LoggerFactory.getLogger(OrderExpirationCheck.class);

    private static final long ORDER_CHECK_PERIOD_MS = 1_800_000;

    private final OrderController orderController;

    @Autowired
    public OrderExpirationCheck(OrderController orderController) {
        this.orderController = orderController;
    }

    /**
     * Deletes old order every half an our. (Worst case scenario is that an order will last in the database for an hour
     * which is why there's a check in the payment call as well)
     */
    @Scheduled(fixedDelay = ORDER_CHECK_PERIOD_MS)
    public void expirationCheck() {
        orderController.checkExpirations();
        logger.info("Expiration check performed.");
    }

}
