package com.rohlik.demo.db.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Payment {

    private final Long orderId;

    private final boolean completed;

    @JsonCreator
    public Payment(@JsonProperty("orderId") Long orderId,
                   @JsonProperty("completed") boolean completed) {
        this.orderId = orderId;
        this.completed = completed;
    }

    public Long getOrderId() {
        return orderId;
    }

    public boolean isCompleted() {
        return completed;
    }
}
