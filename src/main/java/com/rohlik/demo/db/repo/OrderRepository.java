package com.rohlik.demo.db.repo;

import com.rohlik.demo.db.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
}
