package com.rohlik.demo.rest;

import com.rohlik.demo.core.OrderItemController;
import com.rohlik.demo.db.model.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/orderItems", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderItemApi {

    private final OrderItemController orderItemController;

    @Autowired
    public OrderItemApi(OrderItemController orderItemController) {
        this.orderItemController = orderItemController;
    }

    @GetMapping
    public List<OrderItem> getAll() {
        return orderItemController.getAll();
    }
}
