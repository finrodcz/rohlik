package com.rohlik.demo.core;

import com.rohlik.demo.db.entity.ProductEntity;
import com.rohlik.demo.db.model.Product;
import com.rohlik.demo.db.repo.ProductRepository;
import com.rohlik.demo.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductController {

    private final ProductRepository productRepository;

    @Autowired
    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAll() {
        List<ProductEntity> entities = productRepository.findAll();

        return entities.stream().map(Product::fromEntity).collect(Collectors.toList());
    }

    public Product getOne(Long id) {
        ProductEntity entity = productRepository.findById(id).orElseThrow(NotFoundException::new);

        return Product.fromEntity(entity);
    }

    public Product createProduct(Product product) {
        ProductEntity entity = new ProductEntity(
                null,
                product.getName(),
                product.getCount(),
                product.getPrice()
        );

        ProductEntity newEntity = productRepository.save(entity);

        return Product.fromEntity(newEntity);
    }

    public Product updateProduct(Long id, Product product) {
        ProductEntity entity = productRepository.findById(id).orElseThrow(NotFoundException::new);

        entity.setName(product.getName());
        entity.setCount(product.getCount());
        entity.setPrice(product.getPrice());

        return Product.fromEntity(productRepository.save(entity));
    }

}
