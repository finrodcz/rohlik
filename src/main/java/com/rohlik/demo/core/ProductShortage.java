package com.rohlik.demo.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductShortage {

    private final Long productId;

    private final int shortage;

    @JsonCreator
    public ProductShortage(@JsonProperty("productId") Long productId,
                           @JsonProperty("shortage") int shortage) {
        this.productId = productId;
        this.shortage = shortage;
    }

    public Long getProductId() {
        return productId;
    }

    public int getShortage() {
        return shortage;
    }
}
