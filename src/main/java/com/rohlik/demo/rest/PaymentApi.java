package com.rohlik.demo.rest;


import com.rohlik.demo.core.PaymentController;
import com.rohlik.demo.db.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/payments", produces = MediaType.APPLICATION_JSON_VALUE)
public class PaymentApi {

    private final PaymentController paymentController;

    @Autowired
    public PaymentApi(PaymentController paymentController) {
        this.paymentController = paymentController;
    }

    @PostMapping
    private Payment createPayment(@Valid @RequestBody Payment payment) {
        return paymentController.createPayment(payment);
    }

}
