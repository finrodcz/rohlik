package com.rohlik.demo.db.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rohlik.demo.db.entity.OrderEntity;

import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Order {

    private final Long id;

    private final Instant orderTime;

    private final List<OrderItem> orderItems;

    @PositiveOrZero
    private final BigDecimal price;

    private final boolean completed;

    @JsonCreator
    public Order(@JsonProperty("id") Long id,
                 @JsonProperty("orderTime") Instant orderTime,
                 @JsonProperty("orderItems") List<OrderItem> orderItems,
                 @JsonProperty("price") BigDecimal price,
                 @JsonProperty("completed") boolean completed) {
        this.id = id;
        this.orderTime = orderTime;
        this.orderItems = orderItems;
        this.price = price;
        this.completed = completed;
    }

    public Instant getOrderTime() {
        return orderTime;
    }

    public Long getId() {
        return id;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public static Order fromEntity(OrderEntity entity) {
        return new Order(
                entity.getId(),
                entity.getOrderTime(),
                entity.getOrderItems().stream().map(OrderItem::fromEntity).collect(Collectors.toList()),
                entity.getPrice(),
                entity.isOrderComplete());
    }
}
