package com.rohlik.demo.db.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rohlik.demo.db.entity.OrderItemEntity;

import javax.validation.constraints.Positive;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class OrderItem {

    private final Long id;

    private final Long productId;

    @Positive(message = "Cannot order zero of a product.")
    private final int count;

    @JsonCreator
    public OrderItem(@JsonProperty("id") Long id,
                     @JsonProperty("productId") Long productId,
                     @JsonProperty("count") int count) {
        this.id = id;
        this.productId = productId;
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public Long getProductId() {
        return productId;
    }

    public int getCount() {
        return count;
    }

    public static OrderItem fromEntity(OrderItemEntity entity) {
        return new OrderItem(
                entity.getId(),
                entity.getProduct().getId(),
                entity.getCount()
        );
    }
}
