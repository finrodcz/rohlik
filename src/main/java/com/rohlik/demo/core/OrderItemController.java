package com.rohlik.demo.core;

import com.rohlik.demo.db.model.OrderItem;
import com.rohlik.demo.db.repo.OrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderItemController {

    private final OrderItemRepository orderItemRepository;

    @Autowired
    public OrderItemController(OrderItemRepository orderItemRepository) {
        this.orderItemRepository = orderItemRepository;
    }

    @Transactional
    public List<OrderItem> getAll() {
        return orderItemRepository.findAll().stream()
                .map(OrderItem::fromEntity)
                .collect(Collectors.toList());
    }

}
