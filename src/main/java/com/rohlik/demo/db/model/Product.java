package com.rohlik.demo.db.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rohlik.demo.db.entity.ProductEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Product {

    private final Long id;

    @NotBlank(message = "Name cannot be blank.")
    private final String name;

    @NotNull(message = "Count parameter is mandatory.")
    @PositiveOrZero(message = "Count must be positive or zero.")
    private final int count;

    private final int ordered;

    @NotNull(message = "Price parameter is mandatory.")
    @PositiveOrZero(message = "Price must be positive or zero.")
    private final BigDecimal price;

    @JsonCreator
    public Product(@JsonProperty("id") Long id,
                   @JsonProperty("name") String name,
                   @JsonProperty("count") int count,
                   @JsonProperty("ordered") int ordered,
                   @JsonProperty("price") BigDecimal price) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.ordered = ordered;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public static Product fromEntity(ProductEntity entity) {
        return new Product(
                entity.getId(),
                entity.getName(),
                entity.getCount(),
                entity.getOrdered(),
                entity.getPrice());
    }

}
