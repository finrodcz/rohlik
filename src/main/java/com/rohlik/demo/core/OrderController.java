package com.rohlik.demo.core;

import com.rohlik.demo.db.entity.OrderEntity;
import com.rohlik.demo.db.entity.OrderItemEntity;
import com.rohlik.demo.db.entity.ProductEntity;
import com.rohlik.demo.db.model.Order;
import com.rohlik.demo.db.model.OrderItem;
import com.rohlik.demo.db.repo.OrderItemRepository;
import com.rohlik.demo.db.repo.OrderRepository;
import com.rohlik.demo.db.repo.ProductRepository;
import com.rohlik.demo.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class OrderController {

    private final OrderRepository orderRepository;

    private final OrderItemRepository orderItemRepository;

    private final ProductRepository productRepository;

    private final EntityManager entityManager;

    @Autowired
    public OrderController(OrderRepository orderRepository,
                           OrderItemRepository orderItemRepository,
                           ProductRepository productRepository,
                           EntityManager entityManager) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.productRepository = productRepository;
        this.entityManager = entityManager;
    }

    @Transactional
    public List<Order> getAll() {
        List<OrderEntity> entities = orderRepository.findAll();

        return entities.stream().map(Order::fromEntity).collect(Collectors.toList());
    }

    @Transactional
    public Order getOne(Long id) {
        OrderEntity orderEntity = orderRepository.findById(id).orElseThrow(NotFoundException::new);

        return Order.fromEntity(orderEntity);
    }

    @Transactional
    public ResponseEntity<Object> createOrder(Order order) throws ResponseStatusException {
        OrderEntity orderEntity = new OrderEntity(
                null,
                Instant.now(),
                Collections.emptyList(),
                BigDecimal.ZERO
        );

        // Get list of product ids
        Set<Long> productIds = order.getOrderItems().stream()
                .map(OrderItem::getProductId)
                .collect(Collectors.toSet());

        // Get products with single query
        Map<Long, ProductEntity> products = productRepository.findAllById(productIds).stream()
                .collect(Collectors.toMap(ProductEntity::getId, Function.identity(), (a, b) -> b));

        // checks validity of the products
        checkProducts(productIds, products);

        // Assign products to order items and check availability
        List<ProductShortage> productShortages = new ArrayList<>();
        List<OrderItemEntity> orderItemEntities = order.getOrderItems().stream()
                .map(item -> createOrderItemEntity(item, orderEntity, products))
                .peek(item -> {
                    int availableCount = availableCount(item);
                    if (availableCount < 0) {
                        productShortages.add(new ProductShortage(item.getProduct().getId(), availableCount));
                    }
                })
                .collect(Collectors.toList());

        // If shortage list empty, complete order creation
        if (productShortages.isEmpty()) {
            Order createdOrder = createOrder(orderEntity, orderItemEntities);

            return ResponseEntity.ok().body(createdOrder);
        } else {
            return ResponseEntity.badRequest().body(productShortages);
        }

    }

    @Transactional
    public Order cancelOrder(Long id) {
        OrderEntity order = orderRepository.findById(id).orElseThrow(NotFoundException::new);

        this.cancelOrder(order);

        return Order.fromEntity(order);
    }

    @Transactional
    public void checkExpirations() {
        orderRepository.findAll().forEach(this::checkOrderExpiration);
    }

    private void checkOrderExpiration(OrderEntity orderEntity) {
        Instant now = Instant.now();

        if (orderEntity.getOrderTime().isAfter(now)) {
            this.cancelOrder(orderEntity);
        }
    }

    private Order createOrder(OrderEntity orderEntity, List<OrderItemEntity> orderItemEntities) {
        // Adjust values of ordered products and compute total price
        BigDecimal totalPrice = orderItemEntities.stream()
                .peek(item -> {
                    int current = item.getProduct().getOrdered();
                    item.getProduct().setOrdered(current + item.getCount());
                })
                .map(item -> item.getProduct().getPrice().multiply(BigDecimal.valueOf(item.getCount())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        orderEntity.setOrderItems(orderItemRepository.saveAll(orderItemEntities));
        orderEntity.setPrice(totalPrice);

        return Order.fromEntity(orderRepository.save(orderEntity));
    }

    private void checkProducts(Set<Long> productIds, Map<Long, ProductEntity> products) {
        if (productIds.size() != products.size()) {
            List<Long> unrecognizedIds = productIds.stream()
                    .filter(id -> !productRepository.existsById(id))
                    .collect(Collectors.toList());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Unrecognized products in input with ids '%s'.", unrecognizedIds));
        }
    }

    private OrderItemEntity createOrderItemEntity(OrderItem orderItem,
                                                  OrderEntity orderEntity,
                                                  Map<Long, ProductEntity> products) {
        return new OrderItemEntity(
                null,
                orderEntity,
                products.get(orderItem.getProductId()),
                orderItem.getCount()
        );
    }

    private int availableCount(OrderItemEntity entity) {
        int count = entity.getCount();
        int available = entity.getProduct().getCount() - entity.getProduct().getOrdered();

        return available - count;
    }

    private void cancelOrder(OrderEntity orderEntity) {
        orderEntity.getOrderItems().forEach(this::cancelProductOrder);

        orderRepository.delete(orderEntity);
    }

    private void cancelProductOrder(OrderItemEntity orderItemEntity) {
        ProductEntity product = orderItemEntity.getProduct();

        int count = orderItemEntity.getCount();
        int productOrderedCount = product.getOrdered();

        product.setOrdered(productOrderedCount - count);
    }

}
